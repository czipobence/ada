
myCorpus <- tm_map(myCorpus,tolower) #kisbet??ss?? alak??t??s

myCorpus <- tm_map(myCorpus,removePunctuation)  # pontok elt??vol??t??sa

myCorpus <- tm_map(myCorpus,removeNumbers) #sz??mjegyek elt??vol??t??sa

removeURL <-function(x) gsub("http[[:alnum:]]*","",x)
myCorpus <- tm_map(myCorpus, removeURL) #url-k elt??vol??t??sa regul??ris kifejez??s egyez??s seg??ts??g??vel

stopwords("en")
myStopwords <- c(stopwords("english"),"available","via") #alap stopsz??t??r b??v??t??se
myStopwords <- setdiff(myStopwords,c("r","big")) #szavak elt??vol??t??sa a stop szavak k??z??l

myCorpus <- tm_map(myCorpus,removeWords,myStopwords)

#szotovezes es a leggyakoribb behelyettesites
myCorpusCopy <- myCorpus
myCorpusCopy <-tm_map(myCorpus,stemDocument)

myCorpus <-tm_map(myCorpus,stemCompletion, dictionary=myCorpusCopy)
myCorpus <- myCorpusCopy
