library(wordcloud)

m <- as.matrix(tdm)
word.freq <- sort(rowSums(m),decreasing = T)
wordcloud(words=names(word.freq),freq=word.freq,min.freq = 5,random.order = F)

