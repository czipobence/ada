/* Ez egy komment*/
* És itt is egy másik;
Data Distances;
	Inches = 2;
	Centimeters = 2.54 * Inches;
run;

Data Class;
	input Name $ Age Income ;
	datalines;
	Edith 35 34000
	Ben 45 45000
	David 34 35000
run;

Data Carscopy;
	set SASHELP.Cars;
run;

Proc import datafile='/folders/myfolders/Alkalmazott adatelemzés - 2015/cereals.dat'
	dbms=dlm
	out=WORK.Cereals;
	delimiter='09'x;
	getnames=yes;
run;

Proc contents data=Work.Cereals;
run;

Proc print data=Work.Cereals;
run;

proc sql;
	select * from Work.Cereals;
quit;

Proc print data=Work.Cereals;
	format Fiber 8.2;
run;

Proc print data=Work.Cereals label;
	label RATE = 'RATING';
run;

Proc print data=Work.Cereals;
	where Fat > 1;
run;

Proc sort data=Work.Cereals;
	by manuf;
Proc print data=work.Cereals;
	var Name Fat RATING;
	by manuf;
	format Fat comma8.2;
	where Fat > 1 ;
run;

* ------------------------------------------------------------------------------

libname ADA '/folders/myfolders/Alkalmazott adatelemzés - 2015';

Proc import datafile='/folders/myfolders/Alkalmazott adatelemzés - 2015/bank.csv'
	dbms=dlm
	out=ADA.Bank;
	delimiter=';';
	getnames=yes;
run;

proc contents data=ADA.Bank;
run;

proc sql;
	select * from ADA.Bank;
quit;
	
proc sgscatter data=ADA.Bank;
	plot balance*age;
run;

proc sgplot data=ADA.Bank;
	histogram balance;
run;

proc sgplot data=ADA.Bank;
	scatter x=balance y=age / group=education;
run;

proc sgplo
