Feltételes utasítások
IF-THEN
EQ, NE, GT, GE, LT, LE, IN
AND, OR, NOT
IF feltétel_1 THEN utasítás_1 ;
ELSE IF feltétel_2 THEN utasítás_2 ;
ELSE IF feltétel_2 THEN utasítás_3 ;
DO-END
DATA fájlnév ;
IF feltétel THEN
DO ;
SAS_utasítás ;
SAS_utasítás ;
SAS_utasítás ;
SAS_utasítás ;
SAS_utasítás ;
…
END ;
ELSE DO ;
SAS_utasítás ;
SAS_utasítás ;
SAS_utasítás ;
…
END ;
RUN ;
SELECT-WHEN-OTHERWISE-END
SELECT (változó) ;
WHEN (érték_1) utasítás_1 ;
WHEN (érték_2) utasítás_2 ;
…
OTHERWISE utasítás_n ;
END ;
Hurok utasítások
DO-TO-BY-END
DO index=kezdet TO vég BY növekmény;
további_SAS_utasítások
END;
DO WHILE
DO WHILE (logika_kifejezés);
SAS_utasítások
END;
DO UNTIL
DO UNTIL (logikai_kifejezés);
SAS_utasítások
END;
Sorok szűrése
IF
csak DATA step
IF SAS_kifejezés ;
WHERE
DATA és PROC is
LIKE
reguláris kifejezés
% bármilyen és bármennyi
_ egy karakter
=*
hasonló dolgok
CONTAINS vagy ?
tartalmaz egy részkaraktert
IS NULL vagy IS MISSING
. vagy '' is jó
BETWEEN-END
Speciális dolgok
_N_ változó
GOTO
DATA step elejére ugrik a RETURN után
LINK
LINK utasítás utánra ugrik a RETURN után
RETURN
OUTPUT
RETAIN
összegző utasítás
_ERROR_
PUT
_ALL_,_NUMERIC_,_CHARACTER_
FIRST. és LAST.
INPUT, az értékhez rendelt formátumnak megfelelően olvassa be a változót, leggyakrabban a karakteres értékek numerikussá konvertálásához használjuk

Klaszterezés
célja
nem felügyelt tanulás, osztályozással való kapcsolata
klaszterezés
pca
dimenzió redukció
önszerveződő hálók
típusai
hierarchikus klaszterezés
típusai
egyesítő
felosztó
tulajdonságai
visszavonhatatlan
partícionáló
k-means
﻿PCA
