/* Library definialasa */
libname ADA "/folders/myfolders/Alkalmazott adatelemzés - 2015";

proc import datafile='/folders/myfolders/Alkalmazott adatelemzés - 2015/churn.txt'
	dbms=dlm
	out=ADA.Churn;
	delimiter=',';
	getnames=yes;
run;

data tmp;
	set ADA.Churn;
	
	where State =* "S";
run;

data tmp;
	set ADA.Churn;
	
	where Account_Length between 0 and 10;
run;

data tmp;
	set ADA.Churn;
	
	if Account_Length GT 240 then put _N_ ;
run;

data ADA.Churn_cluster (keep = _NUMERIC_);
	if _N_ < 100 then set ADA.Churn; 
run;

data ADA.Churn_cluster (drop = Area_Code NormalizedVMessage dayminsbin);
	set ADA.Churn_cluster;
run;

data ADA.Churn_cluster;
	set ADA.Churn_cluster;
	cust_id = put(_N_, 7.);
run;

proc distance data = ADA.Churn_cluster
	method = Euclid
	out = ADA.Churn_distances;
	id cust_id;
	var interval(_NUMERIC_ / std = range);
run;

proc cluster data=ADA.Churn_distances method=ave outtree=tree
	noprint;
	id cust_id;
run;

proc tree;

/* PCA */

proc princomp data = ADA.Churn_cluster out=ADA.Churn_pcaout;
	var _NUMERIC_;
run;

proc sgplot data = ADA.Churn_pcaout;
	scatter x=prin2 y=prin1;
run;

proc tree data=tree level=.05;
run;

proc tree data=tree nclusters=5 out=tree;
	copy cust_id;
run;

proc sort data = tree;
	by cust_id;
run;

proc sort data = ADA.Churn_pcaout;
	by cust_id;
run;

data temp;
	merge tree ADA.Churn_pcaout;
	by cust_id;
run;

proc sgplot data = TEMP;
	scatter x=prin2 y=prin1 / group=CLUSTER;
run;

/* Normalizálás */
data ADA.Churn_pcainput;
	set ADA.Churn_cluster;
run;

proc stdize data = ADA.Churn_pcainput method = range out=ADA.Churn_pcaout2;
	var _NUMERIC_;
run;

proc princomp data = ADA.Churn_pcaout2 out = ADA.Churn_pcaout3;
	var _NUMERIC_;
run;

proc sgplot data = ADA.Churn_pcaout3;
	scatter x=prin2 y=prin1;
run;

/* k-NN */
proc fastclus data = ADA.Churn_pcainput maxc=5 out=knnoutput;
	var _NUMERIC_;
run;

proc princomp data = knnoutput out = ADA.Churn_pcaout4;
	var Account_Length--CustServ_Calls;
run;

proc sgplot data = ADA.Churn_pcaout4;
	scatter x=prin2 y=prin1 / group=CLUSTER;
run;

proc means data=temp;
	class cluster;
	var _NUMERIC_;
run;	