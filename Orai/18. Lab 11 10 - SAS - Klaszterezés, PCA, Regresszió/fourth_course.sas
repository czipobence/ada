/* Klaszterezesi mintafeladat */

libname ada '/folders/myfolders/Alkalmazott adatelemzés - 2015';

/* Adathalmaz beolvasasa */
proc import datafile='/folders/myfolders/Alkalmazott adatelemzés - 2015/telco.txt'
	out=ada.telco
	dbms=dlm
	replace;
	delimiter=";";
	getnames=yes;
run;

/* Numerikus valtozok levalogatasa */
data ada.telco_num;
	set ada.telco (keep=_NUMERIC_);
run;

/* k-means es PCA hasznalata */
proc fastclus data = ada.telco_num maxc=5 out=knnoutput;
run;

proc sgplot data=knnoutput;
	scatter x=age y=Peak_minute_09 / group=cluster;
run;

/* Valtozok normalizalasa */
proc standard data=ada.telco_num mean=0 std=1 out=knninput;
   var _ALL_;
run;

proc fastclus data = knninput maxc=5 out=knnoutput_norm;
run;

proc sgplot data=knnoutput_norm;
	scatter x=age y=Peak_minute_09 / group=cluster;
run;

/* PCA */
proc princomp data = knninput out = telco_pcaout;
	var _ALL_;
run;

/* A PCA es a klaszterezes eredmenyeinek osszekapcsolasa */
data pca_and_cluster;
	merge telco_pcaout knnoutput_norm;
run;

proc sgplot data = pca_and_cluster;
	scatter x=prin2 y=prin1 / group=CLUSTER;
run;

/* Hierarchikus klaszterezes */

data ada.hierarchinput (keep = _NUMERIC_);
	if _N_ < 1000 then set ada.telco; 
run;

data ada.hierarchinput;
	set ada.hierarchinput;
	cust_id = put(_N_, 7.);
run;

proc distance data = ada.hierarchinput
	method = Euclid
	out = ada.telcodistances;
	id cust_id;
	var interval(_NUMERIC_ / std = range);
run;

proc cluster data=ada.telcodistances method=ave outtree=ada.tree
	noprint;
	id cust_id;
run;

proc tree;

proc princomp data=ada.hierarchinput out=ada.pcaoutput2;
run;

proc tree data=ada.tree nclusters=5 out=ada.tree;
	copy cust_id;
run;

proc sort data = ada.tree;
	by cust_id;
run;

proc sort data = ada.pcaoutput2;
	by cust_id;
run;

data ada.clusters_and_pca2;
	merge ada.pcaoutput2 ada.tree;
	by cust_id;
run;

proc sgplot data=ada.clusters_and_pca2;
	scatter x = Prin1 y = Prin2 / group=cluster;
run;

/* Regresszios mintafeladat */

/* Adatbeolvasas */
proc import datafile='/folders/myfolders/Alkalmazott adatelemzés - 2015/ingatlan_final.csv'
	dbms=dlm
	out=ada.ingatlan
	replace;
	delimiter=";";
	getnames=yes;
run;

/* Linearis regresszio */
proc reg data=ada.ingatlan outest=linregout;
	model Ar=Terulet;
run;

proc reg data=ada.ingatlan outest=linregout plots=all;
	model Ar=Terulet-numeric-Komfort / selection=forward;
run;