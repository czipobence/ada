Churn, also called attrition, is a term used to indicate a customer leaving the service of one company in favor of another company. The data set contains 20 variables worth of information about 3333 customers, along with an indication of whether or not that customer churned (left the company). The variables are as follows:

State: categorical, for the 50 states and the District of Columbia
Account length: integer-valued, how long account has been active
Area code: categorical
Phone number: essentially a surrogate for customer ID
International Plan: dichotomous categorical, yes or no
VoiceMail Plan: dichotomous categorical, yes or no
Number of voice mail messages: integer-valued
Total day minutes: continuous, minutes customer used service during the day
Total day calls: integer-valued
Total day charge: continuous, perhaps based on foregoing two variables
Total evening minutes: continuous, minutes customer used service during the evening
Total evening calls: integer-valued
Total evening charge: continuous, perhaps based on foregoing two variables
Total night minutes: continuous, minutes customer used service during the night
Total night calls: integer-valued
Total night charge: continuous, perhaps based on foregoing two variables
Total international minutes: continuous, minutes customer used service to make international calls
Total international calls: integer-valued
Total international charge: continuous, perhaps based on foregoing two variables
Number of calls to customer service: integer-valued