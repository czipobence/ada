/*	Olvassuk be az adathalmazt */
libname ADA '/folders/myfolders/Alkalmazott adatelemzés - 2015';

proc import datafile='/folders/myfolders/Alkalmazott adatelemzés - 2015/churn.txt'
	dbms=dlm
	out=ADA.Churn;
	delimiter=',';
	getnames=yes;
run;

/*	Nevezzük át az utolsó változót, ez a célváltozó lesz */
data ADA.Churn (rename=(Churn_=Churn));
	set ADA.Churn;
run;

/*	Viszgáljuk meg az összefüggést a Charge, Min és Call változók között */
proc sgscatter data=ADA.Churn;
	matrix Day_Mins Day_Calls Day_Charge / group=Churn;
run;

proc sgscatter data=ADA.Churn;
	matrix Eve_Mins Eve_Calls Eve_Charge / group=Churn;
run;

proc sgscatter data=ADA.Churn;
	matrix Night_Mins Night_Calls Night_Charge / group=Churn;
run;

/*	Zárjuk ki a Charge változókat */
data ADA.Churn (drop = Day_Charge Eve_Charge Night_Charge Intl_Charge);
	set ADA.Churn;
run;

/*	Mi az összefüggés az Area_Code és a State változó között?*/
proc freq data=ADA.Churn;
	tables State*Area_Code;
run;

data ADA.Churn (drop=Area_Code);
	set ADA.Churn;
run;

/* 	Kategóriaváltozók vizsgálata
	
	Mi az összefüggés az Int_l_Plan és a Churn változó között?*/
proc sort data=ADA.Churn;
	by Churn;
run;

proc sgplot data=ADA.Churn;
	vbar Int_l_Plan;
	by Churn;
run;

proc freq data=ADA.Churn;
	tables Int_l_Plan*Churn;
run;

/*	Mi az összefüggés az VMail_Plan és a Churn változó között?*/

proc sgplot data=ADA.Churn;
	vbar VMail_Plan;
	by Churn;
run;

proc freq data=ADA.Churn;
	tables VMail_Plan*Churn;
run;

/*	Mi az összefüggés az CustServ_Calls és a Churn változó között?*/
proc sgplot data=ADA.Churn;
	vbar CustServ_Calls;
	by Churn;
run;

proc sgplot data=ADA.Churn;
	vbar CustServ_Calls / group=Churn;
run;

proc freq data=ADA.Churn;
	tables CustServ_Calls*Churn / plots=freqplot(type=dot scale=percent);
run;

/* Új változók létrehozása */
data ADA.Churn (drop = VMailMessage);
	set ADA.Churn;
	
	NormalizedMessage = VMail_Message / Account_length;
run;

proc sgplot data=ADA.Churn;
	scatter x = Churn y = NormalizedMessage;
run;

/*	Mi az összefüggés az Day_Mins és a Churn változó között?*/
proc sgplot data=ADA.Churn;
	vbar Day_Mins / group=Churn;
run;

data ADA.Churn;
	set ADA.Churn;
	dayminsbin = int(Day_Mins/10);
run;

proc freq data=ADA.Churn;
	tables dayminsbin*Churn / out=ADA.tmp;
run;

proc freq data=ADA.Churn;
	tables dayminsbin / out=ADA.tmp2;
run;

data ADA.tmp3;
	merge ADA.tmp (drop=PERCENT) ADA.tmp2 (drop=PERCENT rename=(COUNT=SUMCOUNT));
	by dayminsbin;
run;

data ADA.tmp3;
	set ADA.tmp3;
	ratio=COUNT/SUMCOUNT;
run;

proc sgplot data=ADA.tmp3;
	scatter x = dayminsbin y = ratio / group=Churn;
run;

/* --------------------------------------------------------------- */

data ADA.Organics2 (rename=(DOB = BIRTHDATE));
	set ADA.Organics (drop = EDATE);
run;

data ADA.Organics2;
	set ADA.Organics2;
	BIRTHYEAR = year(BIRTHDATE);
	AGEINYEAR = year(TODAY()) - BIRTHYEAR;
run;

proc sgplot Data=ADA.Organics2;
	vbar AGEINYEAR;
run;

data ADA.Organics2;
	set ADA.Organics2;
	a = scan(TV_REG,1);
run;

data ADA.Organics2;
	set ADA.Organics2;
	b = find(upcase(TV_REG),'EAST');
run;

data ADA.Organics2;
	set ADA.Organics2;
	c = substr(upcase(TV_REG),1,4);
run;

data ADA.Organics2;
	set ADA.Organics2;
	substr(c,1,3) = 'ADA';
run;

data ADA.Organics2;
	set ADA.Organics2;
	d = compress(c, 'ADA');
run;

proc sort data=ADA.Organics2;
	by CLASS;
run;

proc means data=ADA.Organics2 MEAN;
	output out=ADA.Organics_mean;
	by CLASS;
	var BILL;
run;

data ADA.Organics_mean2 (keep = CLASS AVGBILL);
	set ADA.Organics_mean (rename=(BILL=AVGBILL));
	if _STAT_ EQ 'MEAN';
run;

data ADA.Organics2;
	merge ADA.Organics2 ADA.Organics_mean2;
	by CLASS;
run;

data ADA.Organics2;
	set ADA.Organics2;
	if BILL > AVGBILL then GTAVG = 1;
	else GTAVG = 0;
run;