1. A feladat célja, hogy a mellékelt cenzus adat halmazban elemzzük az USA állampolgárait az adatokban található demográfiai adatok alapján. Olyan geográfiai csoportok létrehozása a cél, amely a bevételek, a háztartás mérete és a népsűrűség alapján homogének és a különböző csoportok jól elkülöníthetőek egymástól.

2. A feladat során az alábbi lépéseket kell megtenned:
	a) a bemeneti változók kiválasztása;
	b) új változó(k) létrehozása a meglévők alapján;
	c) egy partícionáló és egy hierarchicus módszer futtatása;
	d) a létrehozott csoportok vizsgálata.
3. Az adat halmaz változói:
	a. ID postal code of the region
	b. LOCX region longitude
	c. LOCY region latitude
	d. MEANHHSZ average household size in the region
	e. MEDHHINC median household income in the region
	f. REGDENS region population density percentile (1=lowest density, 		100=highest density)
	g. REGPOP number people in the region
4. A lépések részletes leírása

Adatok beolvasása. 

Olvassuk be az adathalmazt és dobjuk ki azokat a változókat, amelyek nem demográfiai jellegűek, hiszen az elemzésünk célja nem geofráfiai. Elemezzük a megmaradt változók hisztogramját és egymáshoz képesti viszonyukat. Van-e bármilyen olyan dolog, amit érdemes kezelni az adathalmazban az elemzés megkezdése előtt?

Változók létrehozása. 

Hozzuk létre a MEdHHInc változó logaritmikus transzformáltját, miután maximalizáltuk annak értékét a következő értékben: átlag(MedHHInc)+3*szórás(MedHHInc). A régi változót ne használjuk fel a továbbiakban.
Milyen egyéb változót lehet létrehozni a meglévőekből?

Klaszterezés.

1) Első lépésben z-normalizáljuk a változóinkat.
2) Futtassunk egy hierarchikus klaszterezést az így kapott adathalmazon.
	i) A kapott eredményeket értékeljük és határozzuk meg az optimális klaszterszámot.
	ii) Ábrázoljuk a változókat és a létrejött klasztereket.
	iii) Teszteljük, hogy a PCA segítségével jobban tudjuk-e ábrázolni a létrejött klasztereket. Mi jellemző a létrejött főkomponensekre?
	iv) Elemezzük a klasztereket a klaszterközéppontok segítségével.
3) Futtassunk egy pratícionáló algoritmust az így kapott adathalmazon.
	i) A kapott eredményeket értékeljük és több futtatással határozzuk meg az optimális klaszterszámot.
	ii) Ábrázoljuk a változókat és a létrejött klasztereket.
	iii) Teszteljük, hogy a PCA segítségével jobban tudjuk-e ábrázolni a létrejött klasztereket. Mi jellemző a létrejött főkomponensekre?
	iv) Elemezzük a klasztereket a klaszterközéppontok segítségével.
4) Az alábbi cikkben sok egyéb módszert is találunk a klaszterezés eredményének szemléltetésére.
Egy dokumentumban foglaljuk össze az elemzés eredményét. A dokumentum tartalmazza a futási eredményekből és ábrákból azt, ami az elemzés szempontjából érdekes. Minden ábra és táblázat néhány mondatban legyen elmagyarázva.
