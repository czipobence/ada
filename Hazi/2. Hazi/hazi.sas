libname HAZI '/folders/myfolders/Hazi';

Proc import datafile='/folders/myfolders/Hazi/census2000.csv'
	dbms=dlm
	out=HAZI.Census;
	getnames=yes;
run;

data HAZI.Census (drop = LocX LocY Id);
	set hazi.Census;
run;
/*
proc sgplot data=hazi.census;
	histogram meanhhsz;
run;

proc sgplot data=hazi.census;
	histogram medhhinc;
run;

proc sgplot data=hazi.census;
	histogram regdens;
run;

proc sgplot data=hazi.census;
	histogram regpop;
run;
*/

/*
proc sgscatter data=Hazi.census;
	matrix meanhhsz medhhinc regdens regpop;
run;*/

/*Proc print data=HAZI.Census;
	where (REGDENS < 1) or (REGPOP < 1) or (MEDHHINC < 1) or (MEANHHSZ <= 0);
run;*/

/* Uj tabla letrehozasa */
data Processed;
	set HAZI.Census;
	if cmiss(of _all_) then delete;
	if ((medhhinc <= 0) or (meanhhsz <=0)) then delete; /* 1nél kevesebben egy lakásban(?) */
	help = 1;
run;

/* Átlag kiszámítása */
proc means data=Processed MEAN;
	output out=atlag(drop=_TYPE_ _FREQ_);
	by help;
	var MEDHHINC MEANHHSZ REGPOP;
run;

/* Átlag kimagicelése */
data atlag2 (keep= MHI_AVG MHS_AVG RP_AVG HELP);
	set atlag (rename=(MEDHHINC = MHI_AVG MEANHHSZ = MHS_AVG REGPOP = RP_AVG));
	if _STAT_ EQ 'MEAN';
run;

/* Szórás kimagicelése */
data szoras (keep= MHI_STD MHS_STD RP_STD HELP);
	set atlag (rename=(MEDHHINC = MHI_STD MEANHHSZ = MHS_STD REGPOP = RP_STD));
	if _STAT_ EQ 'STD';
run;

/* Átlag és szórás visszavarázslása */
data Processed;
	merge Processed atlag2;
	by HELP;
	merge Processed szoras;
	by HELP;
run;

/* maximum beállítása */
data Processed;
	set Processed;
	if (medhhinc > MHI_AVG + 3* MHI_STD) then medhhinc = MHI_AVG + 3* MHI_STD;
	if (medhhinc < MHI_AVG - 3* MHI_STD) then medhhinc = MHI_AVG - 3* MHI_STD;
	if (meanhhsz > 5) then meanhhsz = 5;
	if (meanhhsz < 1) then meanhhsz = 1;
	if (regpop > 60000) then regpop = 60000;
	if (regpop < 50) then regpop = 50;
run;

proc means data=processed;
run;

/* logaritmálás */
data Processed;
	set Processed;
	MedHHInc_log = log10(MEDHHINC);
	RegPop_log = log10(REGPOP);
run;


proc sgplot data=processed;
	histogram regpop;
run;

proc sgplot data=processed;
	histogram regpop_log;
run;
/*
proc sgplot data=processed;
	histogram meanhhsz;
run;

proc sgplot data=processed;
	histogram MEDHHINC_LOG;
run;

*/

/* Nem szukseges oszlopok kidobasa */
data Processed (keep = REGDENS REGPOP MEANHHSZ MEDHHINC_LOG);
	set Processed;
run;

/* Az alábbi négy változót z-normalizáljuk (igazából ez a négy van de nem volt mindig így) */
proc standard data=Processed mean=0 std=1  out=Processed_std;
	var REGDENS MEANHHSZ MEDHHINC_LOG REGPOP;
run;

/* Csak az ellenőrzés kedvéért */
proc means data=Processed_std MEAN;
	output out=atlag_std(drop=_TYPE_ _FREQ_);
run;

proc print data=atlag_std;
run;

proc sgplot data=processed_std;
	histogram meanhhsz;
run;

proc sgplot data=processed_std;
	histogram medhhinc_log;
run;

proc sgplot data=processed_std;
	histogram regdens;
run;

proc sgplot data=processed_std;
	histogram regpop;
run;

/*
DATA processed_std ; 
  SET processed_std ; 
  rnd=RANUNI(-1);
run;

PROC SORT DATA=processed_std; 
  BY rnd;  
run;
*/

/* Random 1000-en végzünk klaszterezés */
data hazi.hierarchinput (keep = _NUMERIC_);
	if _N_ < 1000 then set processed_std (); 
run;	

/* Saját id-ra lesz szükség */
data hazi.hierarchinput;
	set hazi.hierarchinput;
	cust_id = put(_N_, 7.);
run;

proc cluster data=hazi.hierarchinput method=ave print=15 ccc pseudo;
	copy cust_id;
run;

/* Távolságok számítása */
proc distance data = hazi.hierarchinput
	method = Euclid
	out = distances;
	id cust_id;
	var interval(_NUMERIC_ / std = range);
run;

proc cluster data=distances method=ave outtree=hazi.tree
	noprint;
	id cust_id;
run;

/* n darab klaszterbe osztás */
proc tree data=hazi.tree nclusters=8 noprint out=hazi.tree;
	copy cust_id;
run;

/* Rendezzük az eredményt a cust_id alapján */
proc sort data = hazi.tree;
	by cust_id;
run;

/* Összeillesztjük a bemenettel a cust_id mentén */
data clusters;
	merge hazi.hierarchinput hazi.tree;
	by cust_id;
run;

/* kiplottoljuk a regdens és a meanhhsz szerint */
proc sgplot data=clusters;
	scatter x = REGDENS y = MEANHHSZ / group=cluster;
run;

proc sgplot data=clusters;
	scatter x = REGPOP y = MEDHHINC_LOG / group=cluster;
run;

proc sgplot data=clusters;
	scatter x = REGPOP y = MEANHHSZ / group=cluster;
run;

/* PCA */
proc princomp data=hazi.hierarchinput noprint out=hazi.pca;
run;

/* cust_id szerint rendezés */
proc sort data = hazi.pca;
	by cust_id;
run;

/* illesztés */
data clusters_with_pca;
	merge hazi.pca hazi.tree;
	by cust_id;
run;

/* a két leginkább releváns változó szerint plottoljuk */
proc sgplot data=clusters_with_pca;
	scatter x = Prin1 y = Prin2 / group=cluster;
run;

/* Klaszterközéppontok meghatározása */

proc means data=clusters_with_pca MEAN;
	class cluster;
	var _NUMERIC_;
run;

/* bemenet előkészítése */
data hazi.knninput;
	set processed_std;
	cust_id = put(_N_, 7.);
run;

/*KNN sokszor*/

%macro doFASTCLUS;
     %do k= 3 %to 8;
          proc fastclus
               data= hazi.knninput
               out= fcOut
               maxclusters= &k
               summary;
          run;
     %end;
%mend;
%doFASTCLUS

/* kNN alkalmazása */
proc fastclus data = hazi.knninput maxc=5 out=knnoutput;
	var _NUMERIC_;
run;

/* rendezés */
proc sort data= knnoutput;
	by cust_id;
run;

/* a klaszterek ábrázolása */
proc sgplot data=knnoutput;
	scatter x=REGDENS y=MEDHHinC_LOG /group = cluster;
run;

/* PCA */
proc princomp data=hazi.knninput noprint out=hazi.pca_knn;
run;

/* renezés cust_id mentén */
proc sort data = hazi.pca_knn;
	by cust_id;
run;

/*illesztés clust_id mentén*/
data pca_and_knn;
	merge hazi.pca_knn knnoutput;
	by cust_id;
run;

/* a két legrelevánsabb változó mentén plottolás */
proc sgplot data = pca_and_knn;
	scatter x=prin2 y=prin1 / group=CLUSTER;
run;

/* Klaszterközéppontok */

proc means data=pca_and_knn MEAN;
	class cluster;
	var _NUMERIC_;
run;
