Feladatok

Olvasd be a JSON fájlt, és:
Írd ki egy CSV fájlba, fejléccel együtt az adatokat
A json csomag (beolvasás) és csv csomag (kiírás) segítségével
Használd a JSON struktúra ‘columns’ részének ‘id’ mezőit a fejléchez
Hagyd el az aláhúzással (‘_’) kezdődő névvel rendelkező oszlopokat
Milyen oszlopok maradtak meg? - ???
Ezután számold meg a sorokat a kiírt CSV fájlban (végigolvasva azt) - ???
Ehhez nincs szükség semmilyen extra csomagra (de használható bármi)
Véletlen viselkedésből determinisztikus
Válasz egy négy-ötjegyű pozitív egész számot. Melyik ez a szám? - ???
Használd ezt a számot seed értékként minden olyan műveletnél, ahol van valamilyen véletlen viselkedés!
Ezt úgy tudod észrevenni, hogy ott van véletlen viselkedés, ahol az értékek (számértékek, kiválasztott sorok, stb.) két futtatás között eltérnek, egészen addig, amíg seedet meg nem adsz, utána ez determinisztikussá válik.
A random csomag seed függvénye vagy a numpy csomag random részének seed függvénye segíthet ebben, de az adott csomagok implementációitól függ, hogy mi is használ saját seedet (ilyenkor ezt át kellhet adni paraméterként egy-egy függvénynek), mi pedig a fent említett csomagokat.
Használd fel a pandas csomagot és a CSV fájlt az alábbiakhoz:
Olvasd be a fájlt
Ügyelj a megfelelő kódolásra… (encoding=iso-8859-2)
Mi látható a legelső sor ‘ALLAPOT’ oszlopában? - ???
Leválogatás
Printeld ki a DataFrame ‘DRAGABB_TRUE’ oszlopának első 2 elemét. Csak az oszlopot, Series objektumként... (df[‘DRAGABB_TRUE’][:2])
Printeld ki a DataFrame ‘DRAGABB_TRUE’ oszlop nélküli tartalmának utolsó 2 sorát… (df.drop([‘DRAGABB_TRUE’], axis=1)[-2:])
Kategorikus változók és missing értékek
Most (egyelőre csak ilyen bután) hozz létre dummy változókat a legegyszerűbb módon a kategorikus változók helyett
Segítség: DataFrame.get_dummies() függvényével...
Vigyázz, mert a DataFrame nem módosul alapból helyben, a visszatérési érték az érdekes!
Most (egyelőre csak ilyen bután) helyettesítsd a hiányzó értékeket nullával
Segítség: DataFrame.fillna(0) függvényhívással...
Vigyázz, mert a DataFrame nem módosul alapból helyben, a visszatérési érték az érdekes!
Modellezés
Importáld be a megfelelő modult (ensemble) a sci-kit learn (sklearn) csomagból, amivel GradientBoostingClassifier osztályozó eljárát taníthatsz a DRAGABB_TRUE oszlop értékei alapján
Tanítsd ezt a modellt a teljes adathalmazon (default beállításokkal)
Értékeld ki ezt a modellt úgy, hogy kiszámítod az AUC értéket ugyanezeken az adatokon - ???
Ábrázold a ROC görbét matplotlib segítségével - ???
Végezz keresztvalidációt
5 részre bontva az adathalmazt (véletlenszerűen), és minden egyes részen kieértékelve százalékos pontosságot (accuracy) használva egy, a másik négy rész együttesét felhasználva felépített döntési fát.
Milyen pontosságértékek jöttek ki az 5 részen? - ???
Jól jöhet: sklearn.cross_validation.cross_val_score
Futásidő mérés, ábrázolás matplotlibbel...
Két implementációt megadunk, de kellene egy harmadik, ami generátort ad vissza, és belül használja a yield kulcsszót.
test = lambda i: ((sum(map(int, str(i))) % 5) in [0, 2])
def my_func(limit=0, last=0):
   return [i for i in xrange(1, limit+1) if test(i)]
def my_func_filter(limit=0, last=0):
   return filter(test, xrange(1, limit+1))
A függvény eredménye legyen egy olyan objektum, aminek az elemein végigmehetünk egy for ciklussal (iterable). A sorozat elemeiről azt tudjuk, hogy 1-től limitig pontosan azok a számok kerülnek ide növekvő sorrendben, amelyek esetében a számjegyek összegének 5-tel való osztási maradéka 0 vagy 2. A sorozatnak akkor van vége, ha a következő elem már nagyobb lenne, mint a limit értéke.
Tehát az elemek, ha limit = 15: 10.0, 5.5, 4.0, 3.25, 2.8, 2.5, …

Milyen Python verzióban írod a házid? - ???
Mi a len(my_func_gen(12345)) kódrészlet futtatásának eredménye? - ???
Legyen olyan implementáció is, amiben a visszaadott objektum egy generátor, legyen olyan is, ahol nem. (Ezzel a feladatrésszel nem kell foglalkozni, korábbi verzióból maradt itt, nincs értelme.)
Használd az alábbi kódrészletet arra a fenti függvénydefiníció után, hogy a kimérd a futásidőt a felsorolt esetekben (IPython notebookban működik csak a %timeit magic, ez kiváltható más python megoldással is, ha úgy kényelmesebb):
def get_some(iterable):
   pick = 250
   for _ in iterable:
       pick -= 1
       if pick == 0:
           break
iters = xrange(0, 3001, 250)
times = []
times_filter = []
times_gen = []
times_gen_2 = []
for i in iters:
   runtime = %timeit -n 100 -r 3 -o my_func(i)
   times.append(runtime.best)
   runtime = %timeit -n 100 -r 3 -o my_func_filter(i)
   times_filter.append(runtime.best)
   runtime = %timeit -n 100 -r 3 -o my_func_gen(i)
   times_gen.append(runtime.best)
   runtime = %timeit -n 300 -r 3 -o get_some(my_func_gen(i))
   times_gen_2.append(runtime.best)
   print('Round {} completed'.format(i))
Ábrázold az iters változók függvényében a fenti futásidőket az egyes implementációkhoz - ???
Egy implementáció mintapontjai legyenek egyszínűek, összekötheted töröttvonallal...
Az alábbiak mintapontjai legyenek különböző színűek egymástól:
times
times_filter
times_gen
times_gen_2
Próbáld megmagyarázni az, hogy a második kettő miért tér el jelentősen az első kettőtől, illetve egymástól. - ???
Legyenek az ábrán feliratok is (legalább legyen címe).
Jól jöhet:
Érdemes lehet (és szabad is) plusz paramétert megadni a függvényünknek (ha megadunk default értéket), vagy definiálni egy kétparaméteres változatot amit az egyparaméteres használhat… A lényeg, hogy tudjuk a köztes lépéseknél, hogy hol is tartunk:
def my_func_gen(limit=0, last=0)
Beadandó válaszok száma (azaz ahol ??? van)

Összesen 11:
8 rövid szöveges válasz
2 grafikon képként
1 hosszabb szöveges válasz
